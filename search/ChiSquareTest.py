#!/usr/bin/env python3

from copy import deepcopy
from itertools import combinations, chain, permutations
from math import sqrt, log
from scipy.stats import norm, chi2
from search import IndependenceTest
from data import DataSet

import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
import warnings

class ChiSquareTest(IndependenceTest):

    def __init__(self, data, alpha):

        if not data.is_discrete():
            raise TypeError("ChiSquare can only be performed on discrete data")

        self.data = data
        self.variables = data.get_variables()
        self.num_vars = data.get_num_variables()
        self.variable_names = data.get_variable_names()
        self.num_rows = data.get_num_rows()

        node_map = {}

        for i in range(self.num_vars):
            node = self.variables[i]
            node_map[node] = i

        self.variable_map = node_map

        name_map = {}

        for i in range(self.num_vars):
            name = self.variable_names[i]
            name_map[name] = i

        self.name_map = name_map

        self.alpha = alpha

    def is_independent(self, x, y, z):
        # Step 1: Subset the data

        z_ = [self.variable_map[i] for i in z]
        categories_list = [np.unique(self.data[:, i]) for i in z_]  # Obtain the categories of each variable in z
        value_config_list = cartesian_product(categories_list)  # Obtain all the possible value configurations of the conditioning_set (e.g., [[]] if categories_list == [])

        max_categories = int(np.max(self.data)) + 1  # Used to fix the size of the contingency table (before applying Fienberg's method)

        sum_of_chi_square = 0  # initialize a zero chi_square statistic
        sum_of_df = 0  # initialize a zero degree of freedom

        for value_config in range(len(value_config_list)):
            L = list(zip(z_, value_config_list[value_config]))
            sub_data = recursive_and(L)[:, [self.variable_map[x], self.variable_map[y]]]  # obtain the subset dataset (containing only the X, Y columns) with only rows specifed in value_config

            # Step 2: Generate contingency table (applying Fienberg's method)
            ctable = make_ctable(sub_data, max_categories)

            # Step 3: Calculate chi-square statistic and degree of freedom from the contingency table
            row_sum = np.sum(ctable, axis=1)
            col_sum = np.sum(ctable, axis=0)
            expected = np.outer(row_sum, col_sum) / sub_data.shape[0]
            if G_sq == False:
                chi_sq_stat = np.sum(((ctable - expected) ** 2) / expected)
            else:
                div = np.divide(ctable, expected)
                div[div == 0] = 1  # It guarantees that taking natural log in the next step won't cause any error
                chi_sq_stat = 2 * np.sum(ctable * np.log(div))
            df = (ctable.shape[0] - 1) * (ctable.shape[1] - 1)

            sum_of_chi_square += chi_sq_stat
            sum_of_df += df

        # Step 4: Compute p-value from chi-square CDF
        if sum_of_df == 0:
            return 1 > self.alpha
        else:
            return chi2.sf(sum_of_chi_square, sum_of_df) > self.alpha

    def recursive_and(self, L):
        "A helper function for subsetting the data using the conditions in L of the form [(variable, value),...]"
        if len(L) == 0:
            return self.data
        else:
            condition = self.data[:, L[0][0]] == L[0][1]
            i = 1
            while i < len(L):
                new_conjunct = self.data[:, L[i][0]] == L[i][1]
                condition = new_conjunct & condition
                i += 1
            return data[condition]

    def make_ctable(self, D, cat_size):
        x = np.array(D[:, 0], dtype=np.dtype(int))
        y = np.array(D[:, 1], dtype=np.dtype(int))
        bin_count = np.bincount(cat_size * x + y)  # Perform linear transformation to obtain frequencies
        diff = (cat_size ** 2) - len(bin_count)
        if diff > 0:  # The number of cells generated by bin_count can possibly be less than cat_size**2
            bin_count = np.concatenate((bin_count, np.zeros(diff)))  # In that case, we concatenate some zeros to fit cat_size**2
        ctable = bin_count.reshape(cat_size, cat_size)
        ctable = ctable[~np.all(ctable == 0, axis=1)]  # Remove rows consisted entirely of zeros
        ctable = ctable[:, ~np.all(ctable == 0, axis=0)]  # Remove columns consisted entirely of zeros
        return ctable
